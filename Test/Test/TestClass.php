<?php
namespace Test;//névtérbe helyezés

class TestClass {
    const SOME_CONSTANT = 10;
    public $teststring;

    public function __construct()
    {
        $this->teststring = "Hello World!";
    }

    public function myFunction(){
        return "Hi, It's Me!";
    }
}