<?php
use Address\Address;
use Address\ExceptionAddress;
/**
 * Autoloader az osztályok betöltésére
 * @param string $class_name
 */
function __autoload($class_name) {
    $name =  $class_name . ".inc";
    $loadName = str_replace('\\',DIRECTORY_SEPARATOR, $name);
    //echo "name->$class_name | $loadName | ".__LINE__;
    include $loadName;
}

//URL action kiolvasása
$action = filter_input(INPUT_GET, 'act') ?: 'list';
$aid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: false; //célpont azonosító beállítása az URL ből, vagy false

if (!empty($_POST)) {
//hibakezelés az űrlap elemekhez
    $hiba = [];

//címsor 1
    $street_address_1 = filter_input(INPUT_POST, "street_address_1");

    if ($street_address_1 == '') {
        $hiba["street_address_1"] = 'Címsor 1: Kötelező kitölteni!';
    }

//cimsor 2 nem kötelező nem kell hibakezelni
//irsz,város
    $city_name = filter_input(INPUT_POST, "city_name");
    if ($city_name == '') {
        $hiba["city_name"] = 'Város: Kötelező kitölteni!';
    }
    $postal_code = filter_input(INPUT_POST, "postal_code");
    if ($postal_code == '') {
        $hiba["postal_code"] = 'Irányítószám: Kötelező kitölteni!';
    }

//ország név
    $country_name = filter_input(INPUT_POST, "country_name");
    if ($country_name == '') {
        $hiba["country_name"] = 'Ország: Kötelező kitölteni!';
    }

//ha üres hiba tömb, akkor nem volt hiba
    if (empty($hiba)) {
        echo 'nincs hiba';
        //kerüljön be a DB be és vissza a listára
        $test = Address::getInstance(filter_input(INPUT_POST, "address_type_id", FILTER_VALIDATE_INT), $_POST);
        $test->save();
        header("Location:?act=list");
        exit(); //visszairányítás a listára
    }

//    echo '<pre>' . var_export($_POST, true) . '</pre>';
//    echo '<pre>' . var_export($hiba, true) . '</pre>';
}
/*
 * CRUD
 */

$output = "";

switch ($action) {
    case "del":
        if ($aid !== false) {
            try {
                $address = Address::load($aid); //lekérés
                $address->delete(); //törlés
                header("Location:?act=list");
                exit(); //visszairányítás a listára
            } catch (ExceptionAddress $e) {
                echo $e;
            }
        } else {
            echo "Vezérlési hiba";
        }
        break;

    case "new":
        $options = '';
        foreach (Address::$valid_address_types as $k => $v) {
            $options .= '<option value="' . $k . '">' . $v . '</option>';
        }
        //hibák kiírása ha vannak
        if (isset($hiba)) {
            $output = hibaKiir($hiba);
        }
        $output .= '<a href="?">&lt;-vissza</a>
            <form method="post">
    <div class="form-group">
        <label for="address-type">Cím jellege</label>
        <select class="form-control" id="address-type" name="address_type_id">
            ' . $options . '
        </select>
    </div>
    <div class="form-group">
        <label for="street-address-1">Címsor 1</label>
        <input type="text" id="street-address-1" name="street_address_1" class="form-control" placeholder="Teszt utca 234." value="' . (filter_input(INPUT_POST, "street_address_1") ?: "") . '">
    </div>
    <div class="form-group">
        <label for="street-address-2">Címsor 2</label>
        <input type="text" id="street-address-2" name="street_address_2" class="form-control">
    </div>
    <div class="row">
        <div class="form-group col-4">
            <label for="postal-code">irányítószám</label>
            <input type="text" id="postal-code" name="postal_code" class="form-control" placeholder="2000">
        </div>
        <div class="form-group col-4">
            <label for="city-name">Település</label>
            <input type="text" id="city-name" name="city_name" class="form-control" placeholder="Szentendre">
        </div>
        <div class="form-group col-4">
            <label for="subdivision-name">Település rész</label>
            <input type="text" id="subdivision-name" name="subdivision_name" class="form-control" placeholder="...">
        </div>
    </div>
    <div class="form-group">     
        <label for="country-name">Ország</label>
        <input type="text" id="country-name" name="country_name" class="form-control" value="Magyarország">
    </div>
     <button type="submit" class="btn btn-primary">Felvitel</button>
</form>';
        break;
    default://lista
        $addresses = Address::all(); //összes cím lekérése az adatbázisból

        $addressList = '<a href="?act=new">új cím</a>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">irsz,város,címsor 1</th>
      <th scope="col">cím jellege</th>
      <th scope="col">művelet</th>
    </tr>
  </thead>
  <tbody>';
        foreach ($addresses as $address) {
            $addressList .= '<tr>
      <th scope="row">' . $address['address_id'] . '</th>
      <td>' . $address['postal_code'] . ',' . $address['city_name'] . ', ' . $address['street_address_1'] . '</td>
      <td>' . $address['address_type_id'] . '</td>
      <td><a href="?tid=' . $address['address_id'] . '&amp;act=mod">módosít</a> | <a href="?tid=' . $address['address_id'] . '&amp;act=del">töröl</a></td>
    </tr>';
        }
        $addressList .= '</tbody></table>';

        $output = $addressList;
}

echo $output; //kimenet kiírása

function hibaKiir($errors = []) {
    $ret = '';

    if (!empty($errors)) {
        $ret = '<div class="alert alert-danger">'
                . '<ul>';
        foreach ($errors as $error) {
            $ret .= "<li>$error</li>";
        }
        $ret .= '</ul></div>';
    }
    return $ret;
}
