<?php
namespace Address;
/**
 * Ideiglenes (Szállítási) cím bővítés
 */
class AddressTemporary extends Address {

    //display override
    public function display() {
        $output = '<div class="temporary alert alert-success">';
        $output .= parent::display();
        $output .= '</div>';
        
        return $output;
    }
    //Címtipus inicializálása
    protected function _init(){
        $this->_setAddressTypeId(Address::ADDRESS_TYPE_TEMPORARY);
        return;
    }

}
