<?php
namespace Address;
use Database\Database;
use Address\Model;
/**
 * Cím kezelő kezelő osztály
 */
abstract class Address implements Model {

    //osztály állandók
    const ADDRESS_TYPE_RESIDENCE = 1; //állandó
    const ADDRESS_TYPE_BUSINESS = 2; //számlázási
    const ADDRESS_TYPE_TEMPORARY = 3; //ideiglenes
    //hiba kódok
    const ADDRESS_ERROR_NOT_FOUND = 1000; //nincs meg a cim azonosito alapján
    const ADDRESS_ERROR_TYPE_ID_NOT_FOUND = 1001;

    //statikus tömb a címtipusok string megjelenítéséhez ClassName::$var ként osztályszintű tulajdonság

    public static $valid_address_types = [
        Address::ADDRESS_TYPE_RESIDENCE => 'Residence',
        self::ADDRESS_TYPE_BUSINESS => 'Business',
        self::ADDRESS_TYPE_TEMPORARY => 'Temporary'
    ];
    //címsor 1 és 2
    public $street_address_1;
    public $street_address_2;
    //település név
    public $city_name;
    //település rész
    public $subdivision_name;
    //irányítószám
    protected $_postal_code;
    //ország
    public $country_name;
    //cím azonosító (db)
    protected $_address_id;
    //címtipus azonosító
    protected $_address_type_id;
    //timestamps
    protected $_time_created = 12345;
    protected $_time_updated = 67890;

    //Magic Methods
    /**
     * Magic construct
     * akkor fut, ha objektum készül (new)
     * @param array $data
     */
    public function __construct($data = []) {
        //var_dump('fut a konstruktor', $data);
        $this->_time_created = time();
        $this->_init();
        //ha kaptunk asszoiciativ tömbben adatokat, megpróbáljuk felépíteni az objektumot
        if (is_array($data)) {
            if (count($data) > 0) {
                foreach ($data as $attribute => $value) {
                    if (in_array($attribute, [
                                'address_id', 'time_created', 'time_updated'
                            ])) {
                        $attribute = '_' . $attribute;
                    }
                    $this->$attribute = $value;
                }
            }
        } else {
            trigger_error("Nem sikerült a kapott adatokból felépíteni az objektumot...");
        }
    }

    /**
     * Magic __get
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        //echo ('fut a get, emiatt:'.$name);
        //ha nincs postal_code, keresünk
        if (!$this->_postal_code) {
            $this->_postal_code = $this->_postal_code_search();
        }
        //ha létezik védett tulajdonság a név alapján akkor visszaadjuk azt
        $protected_property_name = '_' . $name;
        if (property_exists($this, $protected_property_name)) {
            return $this->$protected_property_name;
        }

        trigger_error("Nem létező vagy védett tulajdonságot próbálsz elérni (__get):  $name");
    }

    public function __set($name, $value) {
        //die('fut a set, ezek miatt: ' . $name . ' - ' . $value);
        if ($name == 'postal_code') {
            $this->$name = $value;
            return;
        }
        if ($name == 'address_type_id') {
            $this->_setAddressTypeId($value);
            return;
        }
        //ha nem kezeltük, trigger
        trigger_error("Nem létező vagy védett tulajdonságot próbálsz megadni (__set):  $name - [$value]");
    }

    /**
     * Objektum kiírása ha echo $object van
     * @return string
     */
    public function __toString() {
        return $this->display();
    }

    //----------------------------------
    /**
     * Irányítószám keresés város és városrész alapján
     * @return string
     * @todo elkészíteni a valós eljárást és lekérdezést
     */
    protected function _postal_code_search() {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódlap illesztése
        $mysqli->set_charset('utf8');
        //adatok védelme
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);
        $qry = "SELECT irsz "
                . " FROM telepulesek "
                . " WHERE varos_nev = '$city_name' "
                . " AND varos_resz = '$subdivision_name' ";
        $result = $mysqli->query($qry);
        $row = $result->fetch_assoc();
        if ($row) {
            return $row['irsz'];
        }

        return 'Nem találtam';
    }

    /**
     * eljárás az objektum kiírására (kiírható stringgé alakítás)
     * @return string
     */
    public function display() {
        $output = "";
        $output .= $this->street_address_1;
        if ($this->street_address_2) {
            $output .= "<br>{$this->street_address_2}";
        }
        $output .= "<br>$this->city_name";
        $output .= "<br>$this->country_name";
        $output .= "<br>$this->postal_code";

        return $output;
    }

    /**
     * címtipus megfelelőségének vizsgálata -statikus eljárás tehát objektum létezése nélkül közvetlen az Osztályból hívható Classname::method()
     * @param int $address_type_id
     * @return boolean
     */
    public static function isValidAddressTypeId($address_type_id) {
        return array_key_exists($address_type_id, self::$valid_address_types);
    }

    /**
     * Címtipus azonosít ellenörzéssel való átvétele a védett tulajdonságba
     * @param int $address_type_id
     * @return void
     */
    protected function _setAddressTypeId($address_type_id) {
        if (self::isValidAddressTypeId($address_type_id)) {
            $this->_address_type_id = $address_type_id;
        }
        return;
    }

    /**
     * destruktor az után fut, miután utoljára használtuk az objektumot
     */
    public function __destruct() {
        //echo '<pre>'.var_export($this,true).'</pre>';
    }

    //inicializálás megkövetelése a bővítésektől
    abstract protected function _init();

    /**
     * Cím betöltése
     * @param int $id
     */
    final public static function load($id) {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódlap illesztése
        $mysqli->set_charset('utf8');
        $qry = "SELECT  * "
                . " FROM addresses "
                . " WHERE address_id = " . (int) $id . "  "
                . " LIMIT 1";
        $result = $mysqli->query($qry) or die($mysqli->error);
        $row = $result->fetch_assoc();
        if ($row) {
            $ret = self::getInstance($row['address_type_id'], $row);
//        echo '<pre>' . var_export($ret, true) . '</pre>';
            return $ret;
        }
        //ha nem találtunk címet, hibát dobunk
        throw new ExceptionAddress("Nincs ilyen azonosítójú cím:[$id]", self::ADDRESS_ERROR_NOT_FOUND);
    }

    /**
     * HF: itt kérném a legpraktikusabb megoldást a nem létező address type id lekezelésére
     * Statikus címpéldányosító
     * @param int $address_type_id
     * @param array $data
     * @return obj
     */
    public static function getInstance($address_type_id = self::ADDRESS_TYPE_RESIDENCE, $data = []) {
        if (self::isValidAddressTypeId($address_type_id)) {
            $class_name = 'Address\Address' . self::$valid_address_types[$address_type_id];
            $reflect = new \ReflectionClass($class_name);
            //var_dump($reflect->getMethods());

            if (!$reflect->isAbstract()) {
                return new $class_name($data);
            }
        }
        throw new ExceptionAddress("A kapott address_type_id alapján nem lehet címet létrehozni ($address_type_id)", self::ADDRESS_ERROR_TYPE_ID_NOT_FOUND);
    }

    /**
     * Cím törlése - hf!
     */
    final public function delete() {
        //adatbázis kapcsolat felépítése
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $qry = "DELETE FROM addresses WHERE address_id = {$this->_address_id} LIMIT 1";
        $mysqli->query($qry);

        return;
    }

    /**
     * Cím mentése - hf!
     * @return inserted_id or NULL if fails
     */
    final public function save() {

        //adatbázis kapcsolat felépítése
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódlap illesztése
        $mysqli->set_charset("utf8");
        //query összeállítása
        //adatok alapján insert query összeállítása
        $qry = "INSERT INTO `addresses` (       
            `street_address_1`,
            `street_address_2`,
            `postal_code`,
            `city_name`,
            `subdivision_name`,
            `address_type_id`,
            `country_name`,
            `time_created`,
            `time_updated`) 
            VALUES(
                '{$mysqli->real_escape_string($this->street_address_1)}',
                '{$mysqli->real_escape_string($this->street_address_2)}',
                '{$this->postal_code}',
                '{$mysqli->real_escape_string($this->city_name)}',
                '{$mysqli->real_escape_string($this->subdivision_name)}',
                {$this->address_type_id},
                '{$mysqli->real_escape_string($this->country_name)}',
                '" . date("Y-m-d H:i:s", $this->time_created) . "',
                '{$this->time_updated}')";
        //query futtatása vagy die a hibával
        $result = $mysqli->query($qry) or die('SNAFU on Save:' . $mysqli->error);
        //a most kapott id megadása az objektumnak
        $id = $mysqli->insert_id;
        if ($id) {
            $this->_address_id = $id;
            return $id; //visszatérünk az id-val
        }
        return null;
    }

    final public function update() {
        
    }
    /**
     * Címek lekérése (lista)
     * @param int $qty
     * @return array
     */
    public function all($qty = NULL){
        //adatbázis kapcsolat felépítése
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódlap illesztése
        $mysqli->set_charset("utf8");
        //query összeállítása
        //adatok alapján insert query összeállítása
        $qry = "SELECT * FROM addresses";
        
        $result = $mysqli->query($qry);
        
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        
        return $rows;
    }

}
