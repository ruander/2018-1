<?php
namespace Address;
/**
 * Állandó lakcím bővítés
 */
class AddressResidence extends Address {

    //redeclare
    //public $country_name = "Austria";

    //method override
    public function display() {
        $output = '<div class="residence alert alert-warning">';
        $output .= parent::display();
        $output .= '</div>';
        
        return $output;
    }
    //Címtipus inicializálása
    protected function _init(){
        $this->_setAddressTypeId(Address::ADDRESS_TYPE_RESIDENCE);
        return;
    }

}
