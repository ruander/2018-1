<?php
namespace Address;
/**
 * Számlázási cím bővítés
 */
class AddressBusiness extends Address {


    //display override
    public function display() {
        $output = '<div class="business alert alert-info">';
        $output .= parent::display();
        $output .= '</div>';
        
        return $output;
    }
    //Címtipus inicializálása
    protected function _init(){
        $this->_setAddressTypeId(Address::ADDRESS_TYPE_BUSINESS);
        return;
    }

}
