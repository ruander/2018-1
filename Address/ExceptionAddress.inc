<?php
namespace Address;
/**
 * Saját kivétel kezelés
 */

class ExceptionAddress extends \Exception {
    
    /**
     * Hibaüzenet formázása
     * @return string
     */
    public function __toString() {
        return "[$this->code] : $this->message";
    }
}
