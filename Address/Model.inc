<?php
namespace Address;
/** 
 * Interface az interakcióknak
 */

interface Model {
    
    //az itt felsorolt eljárásokat az interfacet alkalmazó osztálynak tartalmaznia kell!
    //Minden eljárás publikus
    
    //model betöltése
    public static function load($id);
    
    //model frissítése
    public function update();
    
     //egy model mentése
    public function save();
    
    //model törlése
    public function delete();
    
    
    
    
}
