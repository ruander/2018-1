1. alkalom: szintegyeztető feladat megoldása

2. alkalom: GIT alapok, verziókövetés, BitBucket
	- init, add, commit, status, log, config.
	- git add . bemutatása
	- commit -am (add és message)
	- ssh-keygen bemutatása
	- távoli repo hozzáadása (remote add)
	- push és clone működés bemutatása
	- ágak kezelése (branch, checkout)
