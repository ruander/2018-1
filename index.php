<!doctype html>
<html lang="hu">
    <head>
        <title>Ruander PHP haladó tanfolyam</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>  
        <div class="container">
            <h1 class="text-center">PHP haladó tanfolyam - OOP alapok</h1>
            <div class="row">
                <div class="col">
                    <?php include "form.php"; ?>
                </div>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </body>
</html>