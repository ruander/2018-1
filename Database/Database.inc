<?php
namespace Database;
/**
 * Adatbázis csatlakozás egyke - Singleton
 */

class Database {
    
    //kapcsolat tulajdonság
    private $_connection;
    
    //példány
    private static $_instance;
    
    /**
     * 
     * @return obj
     */
    public static function getInstance(){
        if(!self::$_instance){
            self::$_instance = new self;
        }
        
        return self::$_instance;
    }
    public function __construct() {
        //kapcsolat felépítése
        $this->_connection = new \mysqli('localhost','root','','hgy_oop');
        
        //hibakezelés
        if(mysqli_connect_error()){
            trigger_error('Nem sikerült az adatbázis kapcsolat: '.mysqli_connect_error(),E_USER_ERROR);
        }  
    }
    
    /**
     * Védett kapcsolat tulajdonság kiadása
     * @return object
     */
    public function getConnection(){
        return $this->_connection;
    }
    
    /**
     * klónozás védelem - nem overrideolható (final)
     */
    final public function __clone(){}
}
