<?php

/**
 * Autoloader az osztályok betöltésére
 * @param string $class_name
 */
function __autoload($class_name) {
    include "class." . $class_name . ".inc";
}

echo '<h2>Residence példány</h2>';
$data = [
    'street_address_1' => "Nagy tér 42.",
    'city_name' => "Szigetszentmiklós",
        //'country_name' => "Magyarország",
];
$address_residence = new AddressResidence($data);

echo $address_residence->display();
echo '<pre>' . var_export($address_residence, true) . '</pre>';


echo '<h2>Business példány</h2>';
$data = [
    'street_address_1' => "Kis dűlő 122.",
    'city_name' => "Balatonfenyves",
    'country_name' => "Magyarország",
];
$address_business = new AddressBusiness($data);

echo $address_business->display();
echo '<pre>' . var_export($address_business, true) . '</pre>';

echo '<h2>Temporary példány</h2>';
$data = [
    'street_address_1' => "Kis dűlő 122.",
    'city_name' => "Balatonfenyves",
    'country_name' => "Magyarország",
];
$address_temporary = new AddressTemporary($data);

echo $address_temporary->display();
echo '<pre>' . var_export($address_temporary, true) . '</pre>';

echo '<h2>Cím betöltése DB ből példány</h2>';
try {
    $address_db = Address::load(1);
    echo '<pre>' . var_export($address_db, true) . '</pre>';
    echo $address_db->display();
} catch (ExceptionAddress $e) {
    echo $e;
}

echo '<h2>Cím készítése getInstance -el</h2>';
$data = [
    'street_address_1' => "Várkerület 122.",
    'city_name' => "Sopron",
    'country_name' => "Magyarország",
];
try {
    $address_business = Address::getInstance(2, $data);
    //echo $address_business;
    $test_id = $address_business->save();//mentés tesztelése
    echo $address_business;
} catch (ExceptionAddress $e) {
    echo $e;
}
//visszatöltés ellenörzése
try {
    $address_test = Address::load($test_id);
    echo '<pre>' . var_export($address_test, true) . '</pre>';
    echo $address_test->display();
} catch (ExceptionAddress $e) {
    echo $e;
}
